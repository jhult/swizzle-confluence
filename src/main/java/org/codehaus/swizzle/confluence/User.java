/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class User extends MapObject {

	public User() {
		super();
	}

	public User(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * the email address of this user
	 */
	public String getEmail() {
		return getString("email");
	}

	/**
	 * the full name of this user
	 */
	public String getFullname() {
		return getString("fullname");
	}

	/**
	 * the username of this user
	 */
	public String getName() {
		return getString("name");
	}

	/**
	 * the url to view this user online
	 */
	public String getUrl() {
		return getString("url");
	}

	public void setEmail(final String email) {
		setString("email", email);
	}

	public void setFullname(final String fullname) {
		setString("fullname", fullname);
	}

	public void setName(final String name) {
		setString("name", name);
	}

	public void setUrl(final String url) {
		setString("url", url);
	}

}
