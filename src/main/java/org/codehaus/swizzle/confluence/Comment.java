/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Date;
import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class Comment extends MapObject {

	public Comment() {
		super();
	}

	public Comment(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * notated content of the comment (use renderContent to render)
	 */
	public String getContent() {
		return getString("content");
	}

	/**
	 * creation date of the attachment
	 */
	public Date getCreated() {
		return getDate("created");
	}

	/**
	 * creator of the attachment
	 */
	public String getCreator() {
		return getString("creator");
	}

	/**
	 * numeric id of the comment
	 */
	public String getId() {
		return getString("id");
	}

	/**
	 * page ID of the comment
	 */
	public String getPageId() {
		return getString("pageId");
	}

	/**
	 * title of the comment
	 */
	public String getTitle() {
		return getString("title");
	}

	/**
	 * url to view the comment online
	 */
	public String getUrl() {
		return getString("url");
	}

	public void setContent(final String content) {
		setString("content", content);
	}

	public void setCreated(final Date created) {
		setDate("created", created);
	}

	public void setCreator(final String creator) {
		setString("creator", creator);
	}

	public void setId(final String id) {
		setString("id", id);
	}

	public void setPageId(final String pageId) {
		setString("pageId", pageId);
	}

	public void setTitle(final String title) {
		setString("title", title);
	}

	public void setUrl(final String url) {
		setString("url", url);
	}

	@Override
	public Map<String, Object> toRawMap() {
		final Map<String, Object> map = super.toRawMap();
		map.put("created", getCreated());
		return map;
	}
}
