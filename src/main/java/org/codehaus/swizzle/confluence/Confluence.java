package org.codehaus.swizzle.confluence;

import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.naming.directory.SearchResult;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcAhcTransportFactory;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClientException;

/**
 * @version $Revision$ $Date$
 */
public class Confluence {
	private final XmlRpcClient client;
	private String token;
	protected boolean sendRawData;

	public Confluence(String endpoint) throws MalformedURLException {

		if (endpoint.endsWith("/")) {
			endpoint = endpoint.substring(0, endpoint.length() - 1);
		}

		if (!endpoint.endsWith("/rpc/xmlrpc")) {
			endpoint += "/rpc/xmlrpc";
		}

		final XmlRpcClientConfigImpl clientConfig = new XmlRpcClientConfigImpl();
		clientConfig.setServerURL(new URL(endpoint));
		clientConfig.setEnabledForExtensions(true);
		client = new XmlRpcClient();
		client.setTransportFactory(new XmlRpcAhcTransportFactory(client));
		client.setConfig(clientConfig);
	}

	/**
	 * Give anonymous users the permissions {{permissions}} on the space with
	 * the key {{spaceKey}}. (since 2.0)
	 */
	public boolean addAnonymousPermissionsToSpace(final List<?> permissions, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addAnonymousPermissionsToSpace", permissions.toArray(), spaceKey);
		return value.booleanValue();
	}

	/**
	 * Give anonymous users the permission {{permission}} on the space with the
	 * key {{spaceKey}}. (since 2.0)
	 */
	public boolean addAnonymousPermissionToSpace(final String permission, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addAnonymousPermissionToSpace", permission, spaceKey);
		return value.booleanValue();
	}

	/**
	 * add a new attachment to a content entity object. *Note that this uses a
	 * lot of memory -- about 4 times the size of the attachment.*
	 */
	@SuppressWarnings("unchecked")
	public Attachment addAttachment(final long contentId, final Attachment attachment, final byte[] attachmentData) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("addAttachment", new Long(contentId), attachment, attachmentData);
		return new Attachment(data);
	}

	/**
	 * adds a comment to the page.
	 */
	@SuppressWarnings("unchecked")
	public Comment addComment(final Comment comment) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("addComment", comment);
		return new Comment(data);
	}

	/**
	 * add a new group
	 */
	public void addGroup(final String group) throws SwizzleException, ConfluenceException {
		call("addGroup", group);
	}

	/**
	 * Adds a label with the given ID to the object with the given
	 * ContentEntityObject ID.
	 */
	public boolean addLabelById(final long labelId, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addLabelById", new Long(labelId), new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * Adds a label to the object with the given ContentEntityObject ID.
	 */
	public boolean addLabelByName(final String labelName, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addLabelByName", labelName, new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * Adds a label to the object with the given ContentEntityObject ID.
	 */
	public boolean addLabelByNameToSpace(final String labelName, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addLabelByNameToSpace", labelName, spaceKey);
		return value.booleanValue();
	}

	/**
	 * Adds the given label object to the object with the given
	 * ContentEntityObject ID.
	 */
	public boolean addLabelByObject(final Label labelObject, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addLabelByObject", labelObject, new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * Give the entity named {{remoteEntityName}} (either a group or a user) the
	 * permissions {{permissions}} on the space with the key {{spaceKey}}.
	 */
	public boolean addPermissionsToSpace(final List<?> permissions, final String remoteEntityName, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addPermissionsToSpace", permissions.toArray(), remoteEntityName, spaceKey);
		return value.booleanValue();
	}

	/**
	 * Give the entity named {{remoteEntityName}} (either a group or a user) the
	 * permission {{permission}} on the space with the key {{spaceKey}}.
	 */
	public boolean addPermissionToSpace(final String permission, final String remoteEntityName, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("addPermissionToSpace", permission, remoteEntityName, spaceKey);
		return value.booleanValue();
	}

	/**
	 * create a new space, passing in name, key and description.
	 */
	@SuppressWarnings("unchecked")
	public Space addSpace(final Space space) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("addSpace", space);
		return new Space(data);
	}

	/**
	 * add a new user with the given password
	 */
	public void addUser(final User user, final String password) throws SwizzleException, ConfluenceException {
		call("addUser", user, password);
	}

	/**
	 * add a user to a particular group
	 */
	public void addUserToGroup(final String username, final String groupname) throws SwizzleException, ConfluenceException {
		call("addUserToGroup", username, groupname);
	}

	private Object call(final String command) throws SwizzleException, ConfluenceException {
		final Object[] args = {};
		return call(command, args);
	}

	private Object call(final String command, final Object arg1) throws SwizzleException, ConfluenceException {
		final Object[] args = { arg1 };
		return call(command, args);
	}

	private Object call(final String command, final Object arg1, final Object arg2) throws SwizzleException, ConfluenceException {
		final Object[] args = { arg1, arg2 };
		return call(command, args);
	}

	private Object call(final String command, final Object arg1, final Object arg2, final Object arg3) throws SwizzleException, ConfluenceException {
		final Object[] args = { arg1, arg2, arg3 };
		return call(command, args);
	}

	private Object call(final String command, final Object arg1, final Object arg2, final Object arg3, final Object arg4) throws SwizzleException, ConfluenceException {
		final Object[] args = { arg1, arg2, arg3, arg4 };
		return call(command, args);
	}

	private Object call(final String command, final Object[] args) throws SwizzleException {
		for (int i = 0; i < args.length; i++) {
			final Object arg = args[i];
			if (arg instanceof MapObject) {
				final MapObject map = (MapObject) arg;
				if (sendRawData) {
					args[i] = map.toRawMap();
				} else {
					args[i] = map.toMap();
				}
			}
		}
		Object[] vector;
		if (!command.equals("login")) {
			vector = new Object[args.length + 1];
			vector[0] = token;
			System.arraycopy(args, 0, vector, 1, args.length);
		} else {
			vector = args;
		}
		try {
			return client.execute("confluence2." + command, vector);
		} catch (final XmlRpcClientException e) {
			throw new SwizzleException(e.getMessage(), e.linkedException);
		} catch (final XmlRpcException e) {
			e.printStackTrace(System.out);
			throw new ConfluenceException(e.getMessage(), e.linkedException);
		}
	}

	/**
	 * changes the current user's password
	 */
	public boolean changeMyPassword(final String oldPass, final String newPass) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("changeMyPassword", oldPass, newPass);
		return value.booleanValue();
	}

	/**
	 * changes the specified user's password
	 */
	public boolean changeUserPassword(final String username, final String newPass) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("changeUserPassword", username, newPass);
		return value.booleanValue();
	}

	/**
	 * deactivates the specified user
	 */
	public boolean deactivateUser(final String username) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("deactivateUser", username);
		return value.booleanValue();
	}

	/**
	 * edits the details of a user
	 */
	public boolean editUser(final User remoteUser) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("editUser", remoteUser);
		return value.booleanValue();
	}

	/**
	 * exports a Confluence instance and returns a String holding the URL for
	 * the download. The boolean argument indicates whether or not attachments
	 * ought to be included in the export.
	 */
	public String exportSite(final boolean exportAttachments) throws SwizzleException, ConfluenceException {
		return (String) call("exportSite", new Boolean(exportAttachments));
	}

	/**
	 * exports a space and returns a String holding the URL for the download.
	 * The export type argument indicates whether or not to export in XML, PDF,
	 * or HTML format - use "TYPE_XML", "TYPE_PDF", or "TYPE_HTML" respectively.
	 * Also, using "all" will select TYPE_XML.
	 */
	public String exportSpace(final String spaceKey, final String exportType) throws SwizzleException, ConfluenceException {
		return (String) call("exportSpace", spaceKey, exportType);
	}

	/**
	 * returns all registered users as Strings
	 */
	public List<Object> getActiveUsers(final boolean viewAll) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getActiveUsers", new Boolean(viewAll));
		return Arrays.asList(vector);
	}

	/**
	 * returns all the ancestors (as {@link PageSummary} instances) of this page
	 * (parent, parent's parent etc).
	 */
	public List<PageSummary> getAncestors(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getAncestors", pageId);
		return toList(vector, PageSummary.class);
	}

	/**
	 * get information about an attachment.
	 */
	@SuppressWarnings("unchecked")
	public Attachment getAttachment(final String pageId, final String fileName, final String versionNumber) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getAttachment", pageId, fileName, versionNumber);
		return new Attachment(data);
	}

	/**
	 * get the contents of an attachment.
	 */
	public byte[] getAttachmentData(final String pageId, final String fileName, final String versionNumber) throws SwizzleException, ConfluenceException {
		return (byte[]) call("getAttachmentData", pageId, fileName, versionNumber);
	}

	/**
	 * returns all the {@link Attachment}s for this page (useful to point users
	 * to download them with the full file download URL returned).
	 */
	public List<Attachment> getAttachments(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getAttachments", pageId);
		return toList(vector, Attachment.class);
	}

	/**
	 * returns all the {@link BlogEntrySummary} instances in the space.
	 */
	public List<BlogEntrySummary> getBlogEntries(final String spaceKey) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getBlogEntries", spaceKey);
		return toList(vector, BlogEntrySummary.class);
	}

	/**
	 * returns a single BlogEntry.
	 */
	@SuppressWarnings("unchecked")
	public BlogEntry getBlogEntry(final String pageId) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getBlogEntry", pageId);
		return new BlogEntry(data);
	}

	/**
	 * Retrieves a blog post in the Space with the given spaceKey, with the
	 * title 'postTitle' and posted on the day 'dayOfMonth'.
	 */
	@SuppressWarnings("unchecked")
	public BlogEntry getBlogEntryByDayAndTitle(final String spaceKey, final int dayOfMonth, final String postTitle) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getBlogEntryByDayAndTitle", spaceKey, new Integer(dayOfMonth), postTitle);
		return new BlogEntry(data);
	}

	/**
	 * returns all the direct children (as {@link PageSummary} instances) of
	 * this page.
	 */
	public List<PageSummary> getChildren(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getChildren", pageId);
		return toList(vector, PageSummary.class);
	}

	/**
	 * returns an individual comment.
	 */
	@SuppressWarnings("unchecked")
	public Comment getComment(final String commentId) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getComment", commentId);
		return new Comment(data);
	}

	/**
	 * returns all the {@link Comment}s for this page.
	 */
	public List<Comment> getComments(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getComments", pageId);
		return toList(vector, Comment.class);
	}

	/**
	 * returns all the descendents (as {@link PageSummary} instances) of this
	 * page (children, children's children etc).
	 */
	public List<PageSummary> getDescendents(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getDescendents", pageId);
		return toList(vector, PageSummary.class);
	}

	/**
	 * gets all groups as a list of {@link String}s
	 */
	public List<Object> getGroups() throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getGroups");
		return Arrays.asList(vector);
	}

	/**
	 * Returns the content for a given label ID
	 */
	public List<Object> getLabelContentById(final long labelId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getLabelContentById", new Long(labelId));
		return Arrays.asList(vector);
	}

	/**
	 * Returns the content for a given label name.
	 */
	public List<Object> getLabelContentByName(final String labelName) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getLabelContentByName", labelName);
		return Arrays.asList(vector);
	}

	/**
	 * Returns the content for a given Label object.
	 */
	public List<Label> getLabelContentByObject(final Label labelObject) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getLabelContentByObject", labelObject);
		return toList(vector, Label.class);
	}

	/**
	 * Retrieves the {@link Label}s matching the given {{labelName}},
	 * {{namespace}}, {{spaceKey}} or {{owner}}.
	 */
	public List<Label> getLabelsByDetail(final String labelName, final String namespace, final String spaceKey, final String owner) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getLabelsByDetail", labelName, namespace, spaceKey, owner);
		return toList(vector, Label.class);
	}

	/**
	 * Returns all {@link Label}s for the given ContentEntityObject ID
	 */
	public List<Label> getLabelsById(final long objectId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getLabelsById", new Long(objectId));
		return toList(vector, Label.class);
	}

	/**
	 * Returns the most popular {@link Label}s for the Confluence instance, with
	 * a specified maximum number.
	 */
	public List<Label> getMostPopularLabels(final int maxCount) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getMostPopularLabels", new Integer(maxCount));
		return toList(vector, Label.class);
	}

	/**
	 * Returns the most popular {@link Label}s for the given {{spaceKey}}, with
	 * a specified maximum number of results.
	 */
	public List<Label> getMostPopularLabelsInSpace(final String spaceKey, final int maxCount) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getMostPopularLabelsInSpace", spaceKey, new Integer(maxCount));
		return toList(vector, Label.class);
	}

	/**
	 * returns a single Page
	 */
	public Page getPage(final PageSummary summary) throws SwizzleException, ConfluenceException {
		return getPage(summary.getId());
	}

	@SuppressWarnings("unchecked")
	public Page getPage(final String pageId) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getPage", pageId);
		return new Page(data);
	}

	/**
	 * returns a single Page
	 */
	@SuppressWarnings("unchecked")
	public Page getPage(final String spaceKey, final String pageTitle) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getPage", spaceKey, pageTitle);
		return new Page(data);
	}

	/**
	 * returns all the {@link PageHistorySummary} instances - useful for looking
	 * up the previous versions of a page, and who changed them.
	 */
	public List<PageHistorySummary> getPageHistory(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getPageHistory", pageId);
		return toList(vector, PageHistorySummary.class);
	}

	/**
	 * Returns a List of {@link Permission}s representing the permissions set on
	 * the given page.
	 */
	public List<Permission> getPagePermissions(final String pageId) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getPagePermissions", pageId);
		return toList(vector, Permission.class);
	}

	/**
	 * returns all the {@link PageSummary} instances in the space. Doesn't
	 * include pages which are in the Trash. Equivalent to calling
	 * {{Space.getCurrentPages()}}.
	 */
	public List<PageSummary> getPages(final String spaceKey) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getPages", spaceKey);
		return toList(vector, PageSummary.class);
	}

	/**
	 * Returns a List of {@link Permission}s representing the permissions the
	 * current user has for this space (a list of "view", "modify", "comment"
	 * and / or "admin").
	 */
	public List<Object> getPermissions(final String spaceKey) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getPermissions", spaceKey);
		return Arrays.asList(vector);
	}

	/**
	 * Returns a List of {@link Permission}s representing the permissions the
	 * given user has for this space. (since 2.1.4)
	 */
	public List<Permission> getPermissionsForUser(final String spaceKey, final String userName) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getPermissionsForUser", spaceKey, userName);
		return toList(vector, Permission.class);
	}

	/**
	 * Returns the recently used {@link Label}s for the Confluence instance,
	 * with a specified maximum number of results.
	 */
	public List<Label> getRecentlyUsedLabels(final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getRecentlyUsedLabels", new Integer(maxResults));
		return toList(vector, Label.class);
	}

	/**
	 * Returns the recently used {@link Label}s for the given {{spaceKey}}, with
	 * a specified maximum number of results.
	 */
	public List<Label> getRecentlyUsedLabelsInSpace(final String spaceKey, final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getRecentlyUsedLabelsInSpace", spaceKey, new Integer(maxResults));
		return toList(vector, Label.class);
	}

	/**
	 * Returns the {@link Label}s related to the given label name, with a
	 * specified maximum number of results.
	 */
	public List<Label> getRelatedLabels(final String labelName, final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getRelatedLabels", labelName, new Integer(maxResults));
		return toList(vector, Label.class);
	}

	/**
	 * Returns the {@link Label}s related to the given label name for the given
	 * {{spaceKey}}, with a specified maximum number of results.
	 */
	public List<Label> getRelatedLabelsInSpace(final String labelName, final String spaceKey, final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getRelatedLabelsInSpace", labelName, spaceKey, new Integer(maxResults));
		return toList(vector, Label.class);
	}

	/**
	 * retrieve some basic information about the server being connected to.
	 * Useful for clients that need to turn certain features on or off depending
	 * on the version of the server. (Since 1.0.3)
	 */
	@SuppressWarnings("unchecked")
	public ServerInfo getServerInfo() throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getServerInfo");
		return new ServerInfo(data);
	}

	/**
	 * returns a single Space.
	 */
	@SuppressWarnings("unchecked")
	public Space getSpace(final String spaceKey) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getSpace", spaceKey);
		return new Space(data);
	}

	/**
	 * returns List of the space level {@link Permission}s which may be granted.
	 * This is a list of possible permissions to use with
	 * {{addPermissionToSpace}}, below, not a list of current permissions on a
	 * Space.
	 */
	public List<Permission> getSpaceLevelPermissions() throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getSpaceLevelPermissions");
		return toList(vector, Permission.class);
	}

	/**
	 * returns all the {@link SpaceSummary} instances that the current user can
	 * see.
	 */
	public List<SpaceSummary> getSpaces() throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getSpaces");
		return toList(vector, SpaceSummary.class);
	}

	/**
	 * Returns all Spaces that have content labeled with {{labelName}}.
	 */
	public List<Space> getSpacesContainingContentWithLabel(final String labelName) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getSpacesContainingContentWithLabel", labelName);
		return toList(vector, Space.class);
	}

	/**
	 * Returns an array of {@link Space}s that have been labeled with
	 * {{labelName}}.
	 */
	public List<Space> getSpacesWithLabel(final String labelName) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getSpacesWithLabel", labelName);
		return toList(vector, Space.class);
	}

	/**
	 * get a single user
	 */
	@SuppressWarnings("unchecked")
	public User getUser(final String username) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getUser", username);
		return new User(data);
	}

	/**
	 * get a user's current groups as a list of {@link String}s
	 */
	public List<Object> getUserGroups(final String username) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("getUserGroups", username);
		return Arrays.asList(vector);
	}

	/**
	 * Retrieves user information
	 */
	@SuppressWarnings("unchecked")
	public UserInformation getUserInformation(final String username) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("getUserInformation", username);
		return new UserInformation(data);
	}

	/**
	 * checks if a group exists
	 */
	public boolean hasGroup(final String groupname) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("hasGroup", groupname);
		return value.booleanValue();
	}

	/**
	 * checks if a user exists
	 */
	public boolean hasUser(final String username) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("hasUser", username);
		return value.booleanValue();
	}

	/**
	 * imports a space from a compressed XML format (which is usually from an
	 * exported zip)
	 */
	public boolean importSpace(final byte[] importData) throws SwizzleException, ConfluenceException {
		return (boolean) call("importSpace", importData);
	}

	public void login(final String username, final String password) throws SwizzleException, ConfluenceException {
		token = (String) call("login", username, password);
	}

	/**
	 * remove this token from the list of logged in tokens. Returns true if the
	 * user was logged out, false if they were not logged in in the first place
	 * (we don't really need this return, but void seems to kill XML-RPC for me)
	 */
	public boolean logout() throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("logout");
		return value.booleanValue();
	}

	/**
	 * move an attachment to a different content entity object and/or give it a
	 * new name.
	 */
	public boolean moveAttachment(final String originalContentId, final String originalName, final String newContentEntityId, final String newName) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("moveAttachment", originalContentId, originalName, newContentEntityId, newName);
		return value.booleanValue();
	}

	/**
	 * reactivates the specified user
	 */
	public boolean reactivateUser(final String username) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("reactivateUser", username);
		return value.booleanValue();
	}

	/**
	 * Remove all the global and space level permissions for {{groupname}}.
	 */
	public boolean removeAllPermissionsForGroup(final String groupname) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeAllPermissionsForGroup", groupname);
		return value.booleanValue();
	}

	/**
	 * Remove the permission {{permission} from anonymous users on the space
	 * with the key {{spaceKey}}. (since 2.0)
	 */
	public boolean removeAnonymousPermissionFromSpace(final String permission, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeAnonymousPermissionFromSpace", permission, spaceKey);
		return value.booleanValue();
	}

	/**
	 * remove an attachment from a content entity object.
	 */
	public boolean removeAttachment(final String contentId, final String fileName) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeAttachment", contentId, fileName);
		return value.booleanValue();
	}

	/**
	 * removes a comment from the page.
	 */
	public boolean removeComment(final String commentId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeComment", commentId);
		return value.booleanValue();
	}

	/**
	 * remove a group. If {{defaultGroupName}} is specified, users belonging to
	 * {{groupname}} will be added to {{defaultGroupName}}.
	 */
	public boolean removeGroup(final String groupname, final String defaultGroupName) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeGroup", groupname, defaultGroupName);
		return value.booleanValue();
	}

	/**
	 * Removes the label with the given ID from the object with the given
	 * ContentEntityObject ID.
	 */
	public boolean removeLabelById(final long labelId, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeLabelById", new Long(labelId), new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * Removes the given label from the object with the given
	 * ContentEntityObject ID.
	 */
	public boolean removeLabelByName(final String labelName, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeLabelByName", labelName, new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * Removes the given label from the given {{spaceKey}}.
	 */
	public boolean removeLabelByNameFromSpace(final String labelName, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeLabelByNameFromSpace", labelName, spaceKey);
		return value.booleanValue();
	}

	/**
	 * Removes the given label object from the object with the given
	 * ContentEntityObject ID.
	 */
	public boolean removeLabelByObject(final Label labelObject, final long objectId) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeLabelByObject", labelObject, new Long(objectId));
		return value.booleanValue();
	}

	/**
	 * remove a page
	 */
	public void removePage(final String pageId) throws SwizzleException, ConfluenceException {
		call("removePage", pageId);
	}

	/**
	 * Remove the permission {{permission} from the entity named
	 * {{remoteEntityName}} (either a group or a user) on the space with the key
	 * {{spaceKey}}.
	 */
	public boolean removePermissionFromSpace(final String permission, final String remoteEntityName, final String spaceKey) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removePermissionFromSpace", permission, remoteEntityName, spaceKey);
		return value.booleanValue();
	}

	/**
	 * remove a space completely.
	 */
	public Boolean removeSpace(final String spaceKey) throws SwizzleException, ConfluenceException {
		return (Boolean) call("removeSpace", spaceKey);
	}

	/**
	 * delete a user.
	 */
	public boolean removeUser(final String username) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeUser", username);
		return value.booleanValue();
	}

	/**
	 * remove a user from a group.
	 */
	public boolean removeUserFromGroup(final String username, final String groupname) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("removeUserFromGroup", username, groupname);
		return value.booleanValue();
	}

	public String renderContent(final PageSummary page) throws SwizzleException, ConfluenceException {
		return renderContent(page.getSpace(), page.getId());
	}

	public String renderContent(final String spaceKey, final String pageId) throws SwizzleException, ConfluenceException {
		return renderContent(spaceKey, pageId, "");
	}

	/**
	 * returns the HTML rendered content for this page. If 'content' is
	 * provided, then that is rendered as if it were the body of the page
	 * (useful for a 'preview page' function). If it's not provided, then the
	 * existing content of the page is used instead (ie useful for 'view page'
	 * function).
	 */
	public String renderContent(final String spaceKey, final String pageId, final String content) throws SwizzleException, ConfluenceException {
		return (String) call("renderContent", spaceKey, pageId, content);
	}

	/**
	 * Like the above renderContent(), but you can supply an optional hash (map,
	 * dictionary, etc) containing additional instructions for the renderer.
	 * Currently, only one such parameter is supported:
	 */
	public String renderContent(final String spaceKey, final String pageId, final String content, final Map<?, ?> parameters) throws SwizzleException, ConfluenceException {
		return (String) call("renderContent", spaceKey, pageId, content, parameters);
	}

	/**
	 * return a list of {@link SearchResult}s which match a given search query
	 * (including pages and other content types). This is the same as a
	 * performing a parameterised search (see below) with an empty parameter
	 * map.
	 */
	public List<SearchResult> search(final String query, final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("search", query, new Integer(maxResults));
		return toList(vector, SearchResult.class);
	}

	/**
	 * Returns a list of {@link SearchResult}s like the previous search, but you
	 * can optionally limit your search by adding parameters to the parameter
	 * map. If you do not include a parameter, the default is used instead.
	 */
	public List<SearchResult> search(final String query, final Map<?, ?> parameters, final int maxResults) throws SwizzleException, ConfluenceException {
		final Object[] vector = (Object[]) call("search", query, parameters, new Integer(maxResults));
		return toList(vector, SearchResult.class);
	}

	public void sendRawData(final boolean sendRawData) {
		this.sendRawData = sendRawData;
	}

	/**
	 * updates user information
	 */
	public boolean setUserInformation(final UserInformation userInfo) throws SwizzleException, ConfluenceException {
		final Boolean value = (Boolean) call("setUserInformation", userInfo);
		return value.booleanValue();
	}

	/**
	 * add or update a blog entry. For adding, the BlogEntry given as an
	 * argument should have space, title and content fields at a minimum. For
	 * updating, the BlogEntry given should have id, space, title, content and
	 * version fields at a minimum. All other fields will be ignored.
	 */
	@SuppressWarnings("unchecked")
	public BlogEntry storeBlogEntry(final BlogEntry entry) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("storeBlogEntry", entry);
		return new BlogEntry(data);
	}

	/**
	 * add or update a page. For adding, the Page given as an argument should
	 * have space, title and content fields at a minimum. For updating, the Page
	 * given should have id, space, title, content and version fields at a
	 * minimum. The parentId field is always optional. All other fields will be
	 * ignored.
	 */
	@SuppressWarnings("unchecked")
	public Page storePage(final Page page) throws SwizzleException, ConfluenceException {
		final Map<String, Object> data = (Map<String, Object>) call("storePage", page);
		return new Page(data);
	}

	@SuppressWarnings("unchecked")
	private <T> List<T> toList(final Object[] vector, final Class<T> type) throws SwizzleException {
		try {
			final List<T> list = new ArrayList<T>(vector.length);

			final Constructor<T> constructor = type.getConstructor(new Class[] { Map.class });
			for (final Object element : vector) {
				final Map<String, Object> data = (Map<String, Object>) element;
				final T object = constructor.newInstance(new Object[] { data });
				list.add(object);
			}

			return list;

		} catch (final Exception e) {
			throw new SwizzleException(e);
		}
	}

	public boolean willSendRawData() {
		return sendRawData;
	}
}
