/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Date;
import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class Attachment extends MapObject {

	public Attachment() {
		super();
	}

	public Attachment(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * comment for the attachment {color:#cc3300}(Required){color}
	 */
	public String getComment() {
		return getString("comment");
	}

	/**
	 * mime content type of the attachment {color:#cc0000}(Required){color}
	 */
	public String getContentType() {
		return getString("contentType");
	}

	/**
	 * creation date of the attachment
	 */
	public Date getCreated() {
		return getDate("created");
	}

	/**
	 * creator of the attachment
	 */
	public String getCreator() {
		return getString("creator");
	}

	/**
	 * file name of the attachment {color:#cc3300}(Required){color}
	 */
	public String getFileName() {
		return getString("fileName");
	}

	/**
	 * numeric file size of the attachment in bytes
	 */
	public String getFileSize() {
		return getString("fileSize");
	}

	/**
	 * numeric id of the attachment
	 */
	public String getId() {
		return getString("id");
	}

	/**
	 * page ID of the attachment
	 */
	public String getPageId() {
		return getString("pageId");
	}

	/**
	 * title of the attachment
	 */
	public String getTitle() {
		return getString("title");
	}

	/**
	 * url to download the attachment online
	 */
	public String getUrl() {
		return getString("url");
	}

	public void setComment(final String comment) {
		setString("comment", comment);
	}

	public void setContentType(final String contentType) {
		setString("contentType", contentType);
	}

	public void setCreated(final Date created) {
		setDate("created", created);
	}

	public void setCreator(final String creator) {
		setString("creator", creator);
	}

	public void setFileName(final String fileName) {
		setString("fileName", fileName);
	}

	public void setFileSize(final String fileSize) {
		setString("fileSize", fileSize);
	}

	public void setId(final String id) {
		setString("id", id);
	}

	public void setPageId(final String pageId) {
		setString("pageId", pageId);
	}

	public void setTitle(final String title) {
		setString("title", title);
	}

	public void setUrl(final String url) {
		setString("url", url);
	}

	@Override
	public Map<String, Object> toRawMap() {
		final Map<String, Object> map = super.toRawMap();
		map.put("created", getCreated());
		return map;
	}
}
