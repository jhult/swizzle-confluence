/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class BlogEntry extends MapObject {

	public BlogEntry() {
		super();
	}

	public BlogEntry(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * username of the author
	 */
	public String getAuthor() {
		return getString("author");
	}

	/**
	 * the blog entry content
	 */
	public String getContent() {
		return getString("content");
	}

	/**
	 * the id of the blog entry
	 */
	public String getId() {
		return getString("id");
	}

	/**
	 * the number of locks current on this page
	 */
	public int getLocks() {
		return getInt("locks");
	}

	/**
	 * the key of the space that this blog entry belongs to
	 */
	public String getSpace() {
		return getString("space");
	}

	/**
	 * the title of the page
	 */
	public String getTitle() {
		return getString("title");
	}

	/**
	 * the url to view this blog entry online
	 */
	public String getUrl() {
		return getString("url");
	}

	/**
	 * the version number of this blog entry
	 */
	public int getVersion() {
		return getInt("version");
	}

	public void setAuthor(final String author) {
		setString("author", author);
	}

	public void setContent(final String content) {
		setString("content", content);
	}

	public void setId(final String id) {
		setString("id", id);
	}

	public void setLocks(final int locks) {
		setInt("locks", locks);
	}

	public void setSpace(final String space) {
		setString("space", space);
	}

	public void setTitle(final String title) {
		setString("title", title);
	}

	public void setUrl(final String url) {
		setString("url", url);
	}

	public void setVersion(final int version) {
		setInt("version", version);
	}

	@Override
	public Map<String, Object> toRawMap() {
		final Map<String, Object> map = super.toRawMap();
		map.put("version", new Integer(getVersion()));
		map.put("locks", new Integer(getLocks()));
		return map;
	}

}
