/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Date;
import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class UserInformation extends MapObject {

	public UserInformation() {
		super();
	}

	public UserInformation(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * the user description
	 */
	public String getContent() {
		return getString("content");
	}

	/**
	 * the date the user was created
	 */
	public Date getCreationDate() {
		return getDate("creationDate");
	}

	/**
	 * the creator of the user
	 */
	public String getCreatorName() {
		return getString("creatorName");
	}

	/**
	 * the ID of the user
	 */
	public String getId() {
		return getString("id");
	}

	/**
	 * the date the user was last modified
	 */
	public Date getLastModificationDate() {
		return getDate("lastModificationDate");
	}

	/**
	 * the url to view this user online
	 */
	public String getLastModifierName() {
		return getString("lastModifierName");
	}

	/**
	 * the username of this user
	 */
	public String getUsername() {
		return getString("username");
	}

	/**
	 * the version
	 */
	public int getVersion() {
		return getInt("version");
	}

	public void setContent(final String content) {
		setString("content", content);
	}

	public void setCreationDate(final Date creationDate) {
		setDate("creationDate", creationDate);
	}

	public void setCreatorName(final String creatorName) {
		setString("creatorName", creatorName);
	}

	public void setId(final String id) {
		setString("id", id);
	}

	public void setLastModificationDate(final Date lastModificationDate) {
		setDate("lastModificationDate", lastModificationDate);
	}

	public void setLastModifierName(final String lastModifierName) {
		setString("lastModifierName", lastModifierName);
	}

	public void setUsername(final String username) {
		setString("username", username);
	}

	public void setVersion(final int version) {
		setInt("version", version);
	}

	@Override
	public Map<String, Object> toRawMap() {
		final Map<String, Object> map = super.toRawMap();
		map.put("version", new Integer(getVersion()));
		map.put("creationDate", getCreationDate());
		map.put("lastModificationDate", getLastModificationDate());
		return map;
	}
}
