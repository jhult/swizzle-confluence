/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.confluence;

import java.util.Date;
import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class Page extends PageSummary {

	public Page() {
		super();
	}

	public Page(final Map<String, Object> data) {
		super(data);
	}

	/**
	 * the page content
	 */
	public String getContent() {
		return getString("content");
	}

	/**
	 * status of the page (eg current or deleted)
	 */
	public String getContentStatus() {
		return getString("contentStatus");
	}

	/**
	 * timestamp page was created
	 */
	public Date getCreated() {
		return getDate("created");
	}

	/**
	 * username of the creator
	 */
	public String getCreator() {
		return getString("creator");
	}

	/**
	 * timestamp page was modified
	 */
	public Date getModified() {
		return getDate("modified");
	}

	/**
	 * username of the page's last modifier
	 */
	public String getModifier() {
		return getString("modifier");
	}

	/**
	 * the version number of this page
	 */
	public int getVersion() {
		return getInt("version");
	}

	/**
	 * whether the page is current and not deleted
	 */
	public boolean isCurrent() {
		return getBoolean("current");
	}

	/**
	 * whether or not this page is the space's homepage
	 */
	public boolean isHomePage() {
		return getBoolean("homePage");
	}

	public void setContent(final String content) {
		setString("content", content);
	}

	public void setContentStatus(final String contentStatus) {
		setString("contentStatus", contentStatus);
	}

	public void setCreated(final Date created) {
		setDate("created", created);
	}

	public void setCreator(final String creator) {
		setString("creator", creator);
	}

	public void setCurrent(final boolean current) {
		setBoolean("current", current);
	}

	public void setHomePage(final boolean homePage) {
		setBoolean("homePage", homePage);
	}

	public void setModified(final Date modified) {
		setDate("modified", modified);
	}

	public void setModifier(final String modifier) {
		setString("modifier", modifier);
	}

	public void setVersion(final int version) {
		setInt("version", version);
	}

	@Override
	public Map<String, Object> toRawMap() {
		final Map<String, Object> map = super.toRawMap();
		map.put("created", getCreated());
		map.put("modified", getModified());
		map.put("homePage", new Boolean(isHomePage()));
		map.put("current", new Boolean(isCurrent()));
		map.put("version", new Integer(getVersion()));
		return map;
	}
}
