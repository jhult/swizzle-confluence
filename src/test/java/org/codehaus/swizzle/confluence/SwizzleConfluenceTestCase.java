package org.codehaus.swizzle.confluence;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import junit.framework.TestCase;

public abstract class SwizzleConfluenceTestCase extends TestCase {

	private Confluence confluence;
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmmss");

	protected Confluence getConfluence() throws Exception {
		return confluence;
	}

	protected Page getNewTestPage() throws Exception {
		final Page newPage = new Page(new HashMap<String, Object>());
		newPage.setSpace("SWIZZLE");
		newPage.setTitle("Test - " + sdf.format(Calendar.getInstance().getTime()));
		newPage.setParentId(getTestPage().getId());
		newPage.setContent("This is a test");
		confluence.storePage(newPage);
		// Get the new object from the server to ensure to have the page ID
		System.out.println("Page used for tests : [" + newPage.getSpace() + "]:[" + newPage.getTitle() + "]");
		return confluence.getPage(newPage.getSpace(), newPage.getTitle());

	}

	protected Page getTestPage() throws Exception {
		return confluence.getPage("SWIZZLE", "UnitTest Page");

	}

	@Override
	protected void setUp() throws Exception {
		confluence = new Confluence("http://docs.codehaus.org/rpc/xmlrpc");
		confluence.login("swizzle", "swizzle");
	}

	@Override
	protected void tearDown() throws Exception {
		confluence.logout();
	}

}
